# Информация о проекте

jse-04

## Стек технологий

java/Intellij IDEA/Git

## Требования к software

- JDK 1.8

## Команда для запуска проекта

```bash
java -jar ./jse-04.jar
```

## Информация о разработчике

**ФИО**: Баулина Ольга Александровна

**E-MAIL**: golovolomkacom@gmail.com